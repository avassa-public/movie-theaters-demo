# syntax=docker/dockerfile:1

ARG CARGO_TARGET_DIR=/out
ARG ALPINE_VERSION=3.18

FROM --platform=$BUILDPLATFORM rust:latest as chef
WORKDIR /code
RUN cargo install cargo-chef \
    && rm -rf $CARGO_HOME/registry/

FROM --platform=$BUILDPLATFORM chef as prep
COPY --link Cargo.toml Cargo.lock ./
RUN cargo chef prepare

FROM --platform=$BUILDPLATFORM chef as build
ARG CARGO_TARGET_DIR
ENV CARGO_TARGET_DIR=$CARGO_TARGET_DIR
RUN <<EOF
apt-get update
apt-get install -y g++-aarch64-linux-gnu libc6-dev-arm64-cross
rustup target add aarch64-unknown-linux-gnu
rustup toolchain install stable-aarch64-unknown-linux-gnu
EOF
ENV CARGO_TARGET_AARCH64_UNKNOWN_LINUX_GNU_LINKER=aarch64-linux-gnu-gcc \
    CC_aarch64_unknown_linux_gnu=aarch64-linux-gnu-gcc \
    CXX_aarch64_unknown_linux_gnu=aarch64-linux-gnu-g++
COPY --from=prep /code/recipe.json recipe.json
RUN cargo chef cook --release --target x86_64-unknown-linux-gnu --target aarch64-unknown-linux-gnu
RUN --mount=target=. <<EOF
cargo build --release --target x86_64-unknown-linux-gnu --target aarch64-unknown-linux-gnu
mkdir -p ${CARGO_TARGET_DIR}/amd64 ${CARGO_TARGET_DIR}/arm64
for app in curtain-controller digital-assets-manager kettle-popper-manager projector-operations visitor-counter; do
    cp ${CARGO_TARGET_DIR}/x86_64-unknown-linux-gnu/release/$app ${CARGO_TARGET_DIR}/amd64/
    cp ${CARGO_TARGET_DIR}/aarch64-unknown-linux-gnu/release/$app ${CARGO_TARGET_DIR}/arm64/
done
EOF

FROM --platform=$TARGETPLATFORM debian:bookworm-slim as app
RUN apt-get update && apt-get install -y python3 python3-websockets curl procps
ENV SUPD https://api:4646
ENV RUST_LOG info
ENV PYTHONUNBUFFERED 1

FROM app as final
ARG CARGO_TARGET_DIR
ARG TARGETARCH
ARG APP
ENV APP=$APP
ARG VERSION
ENV VERSION=$VERSION
COPY readiness_probe.sh /readiness_probe.sh
COPY --from=build ${CARGO_TARGET_DIR}/${TARGETARCH}/${APP} /usr/local/bin/
CMD $APP
