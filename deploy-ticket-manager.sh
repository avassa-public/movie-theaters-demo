#!/bin/bash

cred_dir=${1:-$HOME/.supctl-topdc}
tenant="ticket-management"

sites="stockholm-sture stockholm-sergel helsingborg-roda-kvarn gothenburg-bergakungen"
for s in $sites; do
	echo "name: $s" | supctl -d $cred_dir create tenants $tenant assigned-sites
done

cred_dir=${2:-$HOME/.supctl-ticket-management}

supctl -d $cred_dir create registry remote <<EOF
name: default
address: registry.gitlab.com
EOF

# supctl -v -d $cred_dir create strongbox identity approle <<EOF
# name: ticket-blipper
# weak-secret-id: true
# EOF

supctl -d $cred_dir create applications < ticket-manager.app.yml
supctl -d $cred_dir create application-deployments < ticket-manager.dep.yml
