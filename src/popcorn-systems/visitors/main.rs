use base64::Engine;
use image::GenericImageView;
use std::sync::Arc;
use tokio::sync::Mutex;

const READY_FILE: &str = "/tmp/ready-code";
const ALERT_FILE: &str = "/tmp/alert";

async fn visitors_main() -> anyhow::Result<()> {
    let mut ar = autoregressive::univariate::Autoregressive::new(100.0, 0.71, &[0.8]);
    let _ = tokio::fs::write(READY_FILE, "0").await;

    let mut interval = tokio::time::interval(std::time::Duration::from_secs(15));
    let mut popped: f64 = 0.0;
    let text_override = std::env::var("TEXT").unwrap_or_else(|_| "Visitors until now".to_string());
    loop {
        interval.tick().await;
        tracing::info!("{text_override} {}", popped as u64);
        if let Ok(username) = std::env::var("USERNAME") {
            tracing::info!("Username: {username}");
        }
        popped += ar.next().unwrap();
    }
}

#[derive(Debug, serde::Deserialize)]
#[serde(rename_all = "kebab-case")]
enum ReadyState {
    Ready,
    Inactive,
    Fail,
}

struct State {
    ready: ReadyState,
    live: bool,
    shutdown_tx: Option<tokio::sync::oneshot::Sender<()>>,
    exit_code: Option<i32>,
    memory: Vec<u8>,
    orig_img: Option<image::DynamicImage>,
    detect_img: Option<image::DynamicImage>,
}

#[tokio::main]
async fn main() -> anyhow::Result<()> {
    tracing_subscriber::fmt::init();
    tracing::info!("starting {}", module_path!());

    tokio::spawn(visitors_main());
    tokio::spawn(alert_main());

    let port: u16 = std::env::var("PORT")
        .ok()
        .and_then(|p| p.parse().ok())
        .unwrap_or(10000);

    tracing::info!("Probes are active on port {port}");

    let (shutdown_tx, shutdown_rx) = tokio::sync::oneshot::channel::<()>();

    let state = Arc::new(Mutex::new(State {
        ready: ReadyState::Ready,
        live: true,
        shutdown_tx: Some(shutdown_tx),
        exit_code: None,
        memory: Default::default(),
        orig_img: None,
        detect_img: None,
    }));

    let app = axum::Router::new()
        .route("/", axum::routing::get(get_root).post(post_root))
        .route("/ready", axum::routing::get(get_ready).post(post_ready))
        .route("/live", axum::routing::get(get_live).post(post_live))
        .route("/crash", axum::routing::post(crash))
        .route("/shutdown", axum::routing::post(shutdown))
        .route("/memory", axum::routing::post(post_memory))
        .layer(axum::extract::Extension(state.clone()));

    let listener = tokio::net::TcpListener::bind(&format!("0.0.0.0:{port}")).await?;
    axum::serve(listener, app)
        .with_graceful_shutdown(shutdown_signal(shutdown_rx))
        .await?;

    if let Some(c) = state.lock().await.exit_code {
        std::process::exit(c);
    }

    Ok(())
}

async fn shutdown_signal(shutdown_rx: tokio::sync::oneshot::Receiver<()>) {
    tokio::select! {
        _ = shutdown_rx => {},
        _ = movies::axum::shutdown_signal() => {},
    }
}

async fn get_ready(
    axum::extract::Extension(state): axum::extract::Extension<Arc<Mutex<State>>>,
) -> axum::http::StatusCode {
    let state = state.lock().await;
    match state.ready {
        ReadyState::Ready => axum::http::StatusCode::OK,
        ReadyState::Inactive => axum::http::StatusCode::NO_CONTENT,
        ReadyState::Fail => axum::http::StatusCode::SERVICE_UNAVAILABLE,
    }
}

#[derive(serde::Deserialize)]
struct Ready {
    ready: ReadyState,
}

async fn post_ready(
    axum::extract::Extension(state): axum::extract::Extension<Arc<Mutex<State>>>,
    axum::extract::Json(payload): axum::extract::Json<Ready>,
) {
    let mut state = state.lock().await;
    state.ready = payload.ready;

    tracing::info!("Setting ready state: {:?}", state.ready);

    let code = match state.ready {
        ReadyState::Ready => "0",
        ReadyState::Inactive => "10",
        ReadyState::Fail => "1",
    };
    let _ = tokio::fs::write(READY_FILE, code).await;
}

async fn get_live(
    axum::extract::Extension(state): axum::extract::Extension<Arc<Mutex<State>>>,
) -> axum::http::StatusCode {
    let state = state.lock().await;
    if state.live {
        axum::http::StatusCode::OK
    } else {
        axum::http::StatusCode::SERVICE_UNAVAILABLE
    }
}

#[derive(serde::Deserialize)]
struct Live {
    live: bool,
}

async fn post_live(
    axum::extract::Extension(state): axum::extract::Extension<Arc<Mutex<State>>>,
    axum::extract::Json(payload): axum::extract::Json<Live>,
) {
    let mut state = state.lock().await;
    state.live = payload.live;
}

async fn crash(axum::extract::Extension(state): axum::extract::Extension<Arc<Mutex<State>>>) {
    let mut state = state.lock().await;
    state.exit_code = Some(123);
    tracing::error!("crash");
    if let Some(shutdown_tx) = state.shutdown_tx.take() {
        shutdown_tx.send(()).expect("shutdown");
    }
}

async fn shutdown(axum::extract::Extension(state): axum::extract::Extension<Arc<Mutex<State>>>) {
    let mut state = state.lock().await;
    tracing::info!("shutdown");
    if let Some(shutdown_tx) = state.shutdown_tx.take() {
        shutdown_tx.send(()).expect("shutdown");
    }
}

#[derive(serde::Deserialize)]
struct Memory {
    mb: usize,
}

async fn post_memory(
    axum::extract::Extension(state): axum::extract::Extension<Arc<Mutex<State>>>,
    axum::extract::Json(payload): axum::extract::Json<Memory>,
) {
    let mut state = state.lock().await;
    tracing::info!("Allocating {} Mb", payload.mb);
    state.memory.resize(payload.mb * 1000_0000, 0xa0);
    state.memory.shrink_to_fit();
}

async fn alert_main() {
    let _ = tokio::fs::write(ALERT_FILE, "0").await;

    // If we have access to the API, otherwise exit
    let avassa_client = match avassa_client::login_helper::login().await {
        Ok(c) => c,
        Err(e) => {
            tracing::error!("Failed to login: {e}");
            return;
        }
    };

    let mut in_alert = false;

    let mut interval = tokio::time::interval(std::time::Duration::from_secs(5));

    const NAME: &str = "too-many-visitors";
    const ID: &str = "too-many-visitors";

    loop {
        interval.tick().await;

        match tokio::fs::read_to_string(ALERT_FILE).await {
            Ok(s) => {
                if !in_alert && s.starts_with('1') {
                    tracing::info!("Alert!");
                    in_alert = true;
                    if let Err(e) = avassa_client::utilities::alerts::alert(
                        &avassa_client,
                        ID,
                        NAME,
                        "Too many visitors in the store",
                        avassa_client::utilities::alerts::Severity::Critical,
                    )
                    .await
                    {
                        tracing::error!("Failed to generate alert: {e}");
                    }
                } else if in_alert && s.starts_with('0') {
                    tracing::info!("Alert cleared!");
                    in_alert = false;
                    if let Err(e) =
                        avassa_client::utilities::alerts::clear_alert(&avassa_client, ID, NAME)
                            .await
                    {
                        tracing::error!("Failed to generate alert: {e}");
                    }
                }
            }
            Err(e) => {
                tracing::error!("Failed to read {ALERT_FILE}: {e}");
            }
        }
    }
}

async fn render_root(state: Arc<Mutex<State>>) -> axum::response::Html<String> {
    let state = state.lock().await;
    let img_tag = if let Some(img) = state.orig_img.as_ref() {
        let mut output = Vec::new();
        img.write_to(
            &mut std::io::Cursor::new(&mut output),
            image::ImageFormat::Png,
        )
        .unwrap();
        let img = base64::engine::general_purpose::STANDARD_NO_PAD.encode(output);
        format!(r#"<img src="data:image/png;base64,{img}"/>"#)
    } else {
        String::new()
    };
    let detect_img_tag = if let Some(img) = state.detect_img.as_ref() {
        let mut output = Vec::new();
        img.write_to(
            &mut std::io::Cursor::new(&mut output),
            image::ImageFormat::Png,
        )
        .unwrap();
        let img = base64::engine::general_purpose::STANDARD_NO_PAD.encode(output);
        format!(r#"<img src="data:image/png;base64,{img}"/>"#)
    } else {
        String::new()
    };
    format!(
        r#"<!DOCTYPE html>
<head>
    <title>Visitor Counter</title>
    <style>
      img {{
      width: 80%;
      margin: 0 auto;
      }}
    </style>
  </head>
<body>
<h1>Detect</h1>
<form method="post" enctype="multipart/form-data">
<input type="file" id="image" name="image" accept="image/png,image/jpeg"/>
<button>Submit</button>
</form>
<div>
{img_tag}
</div>
<div>
{detect_img_tag}
</div>
</body>"#
    )
    .into()
}

async fn get_root(
    axum::extract::Extension(state): axum::extract::Extension<Arc<Mutex<State>>>,
) -> axum::response::Html<String> {
    render_root(state).await
}

async fn post_root(
    axum::extract::Extension(state): axum::extract::Extension<Arc<Mutex<State>>>,
    mut multipart: axum::extract::Multipart,
) -> axum::response::Html<String> {
    while let Some(field) = multipart.next_field().await.unwrap() {
        if let Some("image") = field.name() {
            let img_data = field.bytes().await.unwrap();
            let img = if let Ok(img) = image::ImageReader::new(std::io::Cursor::new(img_data))
                .with_guessed_format()
                .unwrap()
                .decode()
            {
                Some(img)
            } else {
                None
            };
            let mut state = state.lock().await;
            state.orig_img = img;
            state.detect_img = None;
        }
    }
    if let Err(e) = run_detection(state.clone()).await {
        tracing::error!("run_detection: {e}");
    }
    render_root(state).await
}

async fn run_detection(state: Arc<Mutex<State>>) -> anyhow::Result<()> {
    let mut state = state.lock().await;

    let img = state
        .orig_img
        .as_ref()
        .cloned()
        .ok_or(anyhow::anyhow!("No original image"))?;
    let mut img = img.resize_exact(1024, 1024, image::imageops::FilterType::Gaussian);
    let rgb = img.as_rgb8().ok_or(anyhow::anyhow!("Failed to get RGB"))?;

    let rgb_data: Vec<_> = rgb
        .enumerate_rows()
        .map(|row| {
            let row_pixels: Vec<_> = row
                .1
                .map(|p| {
                    let rgb: Vec<_> =
                        p.2 .0
                            .iter()
                            .map(|c| serde_json::Value::Number((*c).into()))
                            .collect();
                    serde_json::Value::Array(rgb)
                })
                .collect();
            serde_json::Value::Array(row_pixels)
        })
        .collect();
    let req = serde_json::json!({
        "instances": [
            serde_json::Value::Array(rgb_data)
            ],
    });

    let c = reqwest::Client::new();

    let tf = std::env::var("TF_SERVING").unwrap_or_else(|_| "ml".to_string());
    tracing::info!("sending request to {tf}");
    let req = c
        .post(format!("http://{tf}:8501/v1/models/resnet:predict"))
        .json(&req)
        .send()
        .await?;

    // let resp: serde_json::Value = req.json().await?;
    let resp: serde_json::Value = req.json().await?;
    std::fs::write("output.json", serde_json::to_vec(&resp)?)?;

    let resp: TFResp = serde_json::from_value(resp)?;
    let labels: Vec<&str> = LABELS.split('\n').collect();

    tracing::info!("drawing boxes");
    let height = img.dimensions().0 as f32;
    let width = img.dimensions().1 as f32;

    tracing::info!("Img: {width}x{height}");
    for pred in resp.predictions {
        for (i, b) in pred.detection_boxes.into_iter().enumerate() {
            if pred.detection_scores[i] < 0.4 {
                continue;
            }

            // let class = labels.get(pred.detection_classes[i] as usize - 1).unwrap_or(&"unknown");
            let class = match labels.get(pred.detection_classes[i] as usize - 1) {
                Some(c) => c,
                None => continue,
            };
            tracing::info!("class: {class}");
            tracing::info!(?b);
            let ymin = (b[0] * height) as i32;
            let xmin = (b[1] * width) as i32;
            let ymax = (b[2] * height) as i32;
            let xmax = (b[3] * width) as i32;
            let w = xmax - xmin;
            let h = ymax - ymin;
            let rect = imageproc::rect::Rect::at(xmin, ymin).of_size(w as u32, h as u32);

            tracing::info!(?rect);
            imageproc::drawing::draw_hollow_rect_mut(
                &mut img,
                rect,
                image::Rgba([255u8, 0, 0, 255]),
            );
        }
    }
    state.detect_img = Some(img);
    Ok(())
}

#[derive(Debug, serde::Deserialize)]
struct Prediction {
    detection_boxes: Vec<[f32; 4]>,
    detection_scores: Vec<f32>,
    detection_classes: Vec<f32>,
}

#[derive(Debug, serde::Deserialize)]
struct TFResp {
    predictions: Vec<Prediction>,
}

const LABELS: &str = "person
bicycle
car
motorcycle
airplane
bus
train
truck
boat
traffic light
fire hydrant
stop sign
parking meter
bench
bird
cat
dog
horse
sheep
cow
elephant
bear
zebra
giraffe
backpack
umbrella
handbag
tie
suitcase
frisbee
skis
snowboard
sports ball
kite
baseball bat
baseball glove
skateboard
surfboard
tennis racket
bottle
wine glass
cup
fork
knife
spoon
bowl
banana
apple
sandwich
orange
broccoli
carrot
hot dog
pizza
donut
cake
chair
couch
potted plant
bed
dining table
toilet
tv
laptop
mouse
remote
keyboard
cell phone
microwave
oven
toaster
sink
refrigerator
book
clock
vase
scissors
teddy bear
hair drier
toothbrush
";
