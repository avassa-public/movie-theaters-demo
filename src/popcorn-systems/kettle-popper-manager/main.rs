use std::sync::Arc;
use tokio::sync::Mutex;

const READY_FILE: &str = "/tmp/ready-code";

async fn no_api_access(
    mut ar: autoregressive::univariate::Autoregressive<f64, 1>,
) -> anyhow::Result<()> {
    tracing::info!("No API access");
    let mut interval = tokio::time::interval(std::time::Duration::from_secs(15));
    let mut popped: f64 = 0.0;
    loop {
        interval.tick().await;
        tracing::info!("{} popcorns popped", popped as u64);
        popped += ar.next().unwrap();
    }
}

async fn popper_main() -> anyhow::Result<()> {
    let mut ar = autoregressive::univariate::Autoregressive::new(100.0, 0.71, &[0.8]);
    let mut popped: f64 = 0.0;
    let _ = tokio::fs::write(READY_FILE, "0").await;

    let client = avassa_client::login_helper::login().await;
    if client.is_err() {
        return no_api_access(ar).await;
    }
    let client = client.unwrap();
    let this_site = avassa_client::utilities::state::site_name(&client).await?;

    let opts = avassa_client::volga::OnNoExists::Create(Default::default());
    let mut producer = client
        .volga_open_producer("sensor-api", movies::POPCORN_TOPIC, opts)
        .await?;

    loop {
        let sv = movies::sensor_common::SensorValue {
            datetime: chrono::Utc::now().to_rfc3339(),
            site: this_site.clone(),
            sensor_name: "popped_popcorns".to_string(),
            value: popped.floor(),
        };

        tracing::debug!("Storing sensor data on Volga: {:#?}", sv);
        let json = serde_json::to_string(&sv)?;
        producer.produce(&json).await?;

        popped += ar.next().unwrap();

        tokio::time::sleep(std::time::Duration::from_secs(5)).await;
    }
}

#[derive(Debug, serde::Deserialize)]
#[serde(rename_all = "kebab-case")]
enum ReadyState {
    Ready,
    Inactive,
    Fail,
}

struct State {
    ready: ReadyState,
    live: bool,
    shutdown_tx: Option<tokio::sync::oneshot::Sender<()>>,
    exit_code: Option<i32>,
    memory: Vec<u8>,
}

#[tokio::main]
async fn main() -> anyhow::Result<()> {
    tracing_subscriber::fmt::init();
    tracing::info!("starting {}", module_path!());

    tokio::spawn(popper_main());

    let port: u16 = std::env::var("PORT")
        .ok()
        .and_then(|p| p.parse().ok())
        .unwrap_or(10000);

    tracing::info!("Listening on {port}");

    let (shutdown_tx, shutdown_rx) = tokio::sync::oneshot::channel::<()>();

    let state = Arc::new(Mutex::new(State {
        ready: ReadyState::Ready,
        live: true,
        shutdown_tx: Some(shutdown_tx),
        exit_code: None,
        memory: Default::default(),
    }));

    let app = axum::Router::new()
        .route("/ready", axum::routing::get(get_ready).post(post_ready))
        .route("/live", axum::routing::get(get_live).post(post_live))
        .route("/crash", axum::routing::post(crash))
        .route("/shutdown", axum::routing::post(shutdown))
        .route("/memory", axum::routing::post(post_memory))
        .layer(axum::extract::Extension(state.clone()));

    let listener = tokio::net::TcpListener::bind(&format!("0.0.0.0:{port}")).await?;
    axum::serve(listener, app)
        .with_graceful_shutdown(shutdown_signal(shutdown_rx))
        .await?;

    if let Some(c) = state.lock().await.exit_code {
        std::process::exit(c);
    }

    Ok(())
}

async fn shutdown_signal(shutdown_rx: tokio::sync::oneshot::Receiver<()>) {
    tokio::select! {
        _ = shutdown_rx => {},
        _ = movies::axum::shutdown_signal() => {},
    }
}

async fn get_ready(
    axum::extract::Extension(state): axum::extract::Extension<Arc<Mutex<State>>>,
) -> axum::http::StatusCode {
    let state = state.lock().await;
    match state.ready {
        ReadyState::Ready => axum::http::StatusCode::OK,
        ReadyState::Inactive => axum::http::StatusCode::NO_CONTENT,
        ReadyState::Fail => axum::http::StatusCode::SERVICE_UNAVAILABLE,
    }
}

#[derive(serde::Deserialize)]
struct Ready {
    ready: ReadyState,
}

async fn post_ready(
    axum::extract::Extension(state): axum::extract::Extension<Arc<Mutex<State>>>,
    axum::extract::Json(payload): axum::extract::Json<Ready>,
) {
    let mut state = state.lock().await;
    state.ready = payload.ready;

    tracing::info!("Setting ready state: {:?}", state.ready);

    let code = match state.ready {
        ReadyState::Ready => "0",
        ReadyState::Inactive => "10",
        ReadyState::Fail => "1",
    };
    let _ = tokio::fs::write(READY_FILE, code).await;
}

async fn get_live(
    axum::extract::Extension(state): axum::extract::Extension<Arc<Mutex<State>>>,
) -> axum::http::StatusCode {
    let state = state.lock().await;
    if state.live {
        axum::http::StatusCode::OK
    } else {
        axum::http::StatusCode::SERVICE_UNAVAILABLE
    }
}

#[derive(serde::Deserialize)]
struct Live {
    live: bool,
}

async fn post_live(
    axum::extract::Extension(state): axum::extract::Extension<Arc<Mutex<State>>>,
    axum::extract::Json(payload): axum::extract::Json<Live>,
) {
    let mut state = state.lock().await;
    state.live = payload.live;
}

async fn crash(axum::extract::Extension(state): axum::extract::Extension<Arc<Mutex<State>>>) {
    let mut state = state.lock().await;
    state.exit_code = Some(123);
    tracing::error!("crash");
    if let Some(shutdown_tx) = state.shutdown_tx.take() {
        shutdown_tx.send(()).expect("shutdown");
    }
}

async fn shutdown(axum::extract::Extension(state): axum::extract::Extension<Arc<Mutex<State>>>) {
    let mut state = state.lock().await;
    tracing::info!("shutdown");
    if let Some(shutdown_tx) = state.shutdown_tx.take() {
        shutdown_tx.send(()).expect("shutdown");
    }
}

#[derive(serde::Deserialize)]
struct Memory {
    mb: usize,
}

async fn post_memory(
    axum::extract::Extension(state): axum::extract::Extension<Arc<Mutex<State>>>,
    axum::extract::Json(payload): axum::extract::Json<Memory>,
) {
    let mut state = state.lock().await;
    tracing::info!("Allocating {} Mb", payload.mb);
    state.memory.resize(payload.mb * 1000_0000, 0xa0);
    state.memory.shrink_to_fit();
}
