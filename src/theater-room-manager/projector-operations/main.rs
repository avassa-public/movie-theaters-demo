use std::convert::TryFrom;

use movies::axum::AppError;
use tracing_opentelemetry::OpenTelemetrySpanExt;

// OTEL_EXPORTER_OTLP_ENDPOINT env var
#[tokio::main]
async fn main() -> anyhow::Result<()> {
    movies::otel::init_tracing("projector-operations".to_string())?;
    tracing::info!("starting {}", module_path!());

    {
        let root = tracing::span!(tracing::Level::INFO, "main");
        let _span = root.enter();
        let app = axum::Router::new()
            .route("/:movie_name", axum::routing::get(start_projector))
            .layer(
                tower::ServiceBuilder::new()
                    .layer(
                        tower_http::trace::TraceLayer::new_for_http()
                            .make_span_with(movies::otel::axum_make_span),
                    )
                    .map_request(movies::otel::axum_accept_trace)
                    .map_request(movies::otel::axum_record_trace_id),
            );

        let listener = tokio::net::TcpListener::bind("0.0.0.0:22000").await?;
        axum::serve(listener, app)
            .with_graceful_shutdown(movies::axum::shutdown_signal())
            .await?;
        tracing::info!("Shutting down");
    }

    opentelemetry::global::shutdown_tracer_provider();
    Ok(())
}

#[tracing::instrument]
async fn start_projector(
    axum::extract::Path(movie_name): axum::extract::Path<String>,
) -> Result<String, AppError> {
    //use opentelemetry::propagation::TextMapPropagator;

    tracing::info!("GET");
    let client = reqwest::Client::builder()
        .connection_verbose(true)
        .build()?;

    let span = tracing::Span::current();
    let context = span.context();
    let mut headers: reqwest::header::HeaderMap = Default::default();
    opentelemetry::global::get_text_map_propagator(|propagator| {
        propagator.inject_context(
            &context,
            &mut opentelemetry_http::HeaderInjector(&mut headers),
        );
    });
    let dest = std::env::var("DIGITAL_ASSETS_MGR").unwrap_or("http://127.0.0.1:21000".to_string());

    let res: movies::ShowMovieResponse = client
        .get(dest)
        .headers(headers)
        .json(&serde_json::json!({
            "movie_name": movie_name,
        }))
        .send()
        .await?
        .json()
        .await?;

    tracing::info!(?res);

    Ok(if res.start {
        format!("Starting movie {movie_name}\n")
    } else {
        format!("Not allowed to start movie {movie_name}\n")
    })
}
