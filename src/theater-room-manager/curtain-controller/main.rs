#[tokio::main]
async fn main() -> anyhow::Result<()> {
    tracing_subscriber::fmt::init();
    tracing::info!("starting {}", module_path!());
    std::future::pending::<()>().await;
    Ok(())
}
