use anyhow::Context;
use futures::future::TryFutureExt;
use movies::axum::AppError;
use sha2::{Digest, Sha256};

async fn print_vault(client: &avassa_client::Client) -> anyhow::Result<()> {
    let site_name = avassa_client::utilities::state::site_name(client).await?;
    let kv = client
        .open_strongbox_vault("operations")
        .and_then(|vault| async move { vault.open_secrets("credentials").await })
        .await
        .context("operations/credentials")?;
    let mut hasher = Sha256::new();
    let username = kv.get("username").unwrap();
    let password = kv.get("password").unwrap();
    hasher.update(password);
    let hash = hasher.finalize();
    let hash = format!("{:x}", hash);
    tracing::info!("{} - {} - {}", site_name, username, &hash[0..16]);
    Ok(())
}

#[tokio::main]
async fn main() -> anyhow::Result<()> {
    movies::otel::init_tracing("digital-assets-manager".to_string())?;
    tracing::info!("starting {}", module_path!());

    tokio::spawn(async {
        let client = avassa_client::login_helper::login().await;
        let mut interval = tokio::time::interval(std::time::Duration::from_secs(5));
        loop {
            interval.tick().await;
            if client.is_err() || print_vault(client.as_ref().unwrap()).await.is_err() {
                tracing::info!("No access");
            }
        }
    });

    {
        let root = tracing::span!(tracing::Level::INFO, "main");
        let _span = root.enter();
        let layer = tower_http::trace::TraceLayer::new_for_http()
            .make_span_with(movies::otel::axum_make_span);
        let app = axum::Router::new()
            .route("/", axum::routing::get(show_movie))
            .layer(
                tower::ServiceBuilder::new()
                    .layer(layer)
                    .map_request(movies::otel::axum_accept_trace)
                    .map_request(movies::otel::axum_record_trace_id),
            );

        let listener = tokio::net::TcpListener::bind("0.0.0.0:21000").await?;
        axum::serve(listener, app)
            .with_graceful_shutdown(movies::axum::shutdown_signal())
            .await?;

        tracing::info!("Stopping");
    }
    opentelemetry::global::shutdown_tracer_provider();
    Ok(())
}

#[tracing::instrument]
async fn show_movie(
    axum::extract::Json(request): axum::extract::Json<movies::ShowMovieRequest>,
) -> Result<axum::response::Json<movies::ShowMovieResponse>, AppError> {
    tracing::info!("GET");

    let now = std::time::SystemTime::now()
        .duration_since(std::time::UNIX_EPOCH)?
        .as_secs();

    Ok(movies::ShowMovieResponse {
        movie_name: request.movie_name,
        timestamp: now,
        start: now % 2 == 0,
    }
    .into())
}
