/*
use crate::theaterops::sensors::sensor_server::Sensor;
use tonic::transport::NamedService;
use tower::Service;

struct SensorServer {
    producer: tokio::sync::Mutex<avassa_client_utilities::volga::Producer>,
}

impl SensorServer {
    async fn new(avassa_client: avassa_client_utilities::Client) -> anyhow::Result<Self> {
        let opts = Default::default();
        let producer = avassa_client
            .volga_open_producer("sensor-api", super::SENSOR_TOPIC, opts)
            .await?;

        Ok(Self {
            producer: tokio::sync::Mutex::new(producer),
        })
    }
}

#[tonic::async_trait]
impl Sensor for SensorServer {
    async fn update(
        &self,
        request: tonic::Request<crate::theaterops::sensors::SensorValue>,
    ) -> std::result::Result<
        tonic::Response<crate::theaterops::sensors::SensorResponse>,
        tonic::Status,
    > {
        log::info!("Sensor::update: {:?}", request);

        let sensor_reading: crate::theaterops::sensor_common::SensorValue =
            request.into_inner().into();
        log::debug!("Storing sensor data on Volga: {:#?}", sensor_reading);
        let json = serde_json::to_string(&sensor_reading)
            .map_err(|e| tonic::Status::unknown(e.to_string()))?;
        let mut producer = self.producer.lock().await;
        producer
            .produce(&json)
            .await
            .map_err(|e| tonic::Status::unknown(format!("{:?}", e)))?;
        Ok(tonic::Response::new(
            crate::theaterops::sensors::SensorResponse {},
        ))
    }
}

#[derive(Clone)]
struct InterceptedService<S> {
    inner: S,
}

impl<S> Service<hyper::Request<hyper::Body>> for InterceptedService<S>
where
    S: Service<hyper::Request<hyper::Body>, Response = hyper::Response<tonic::body::BoxBody>>
        + NamedService
        + Clone
        + Send
        + 'static,
    S::Future: Send + 'static,
{
    type Response = S::Response;
    type Error = S::Error;
    type Future =
        futures::future::BoxFuture<'static, std::result::Result<Self::Response, Self::Error>>;

    fn poll_ready(
        &mut self,
        cx: &mut std::task::Context<'_>,
    ) -> std::task::Poll<std::result::Result<(), Self::Error>> {
        self.inner.poll_ready(cx)
    }

    fn call(&mut self, req: hyper::Request<hyper::Body>) -> Self::Future {
        let mut svc = self.inner.clone();

        Box::pin(async move { svc.call(req).await })
    }
}

impl<S: NamedService> NamedService for InterceptedService<S> {
    const NAME: &'static str = S::NAME;
}
pub(crate) async fn run(client: avassa_client_utilities::Client) -> anyhow::Result<()> {
    log::info!("starting {}", module_path!());
    let addr = "0.0.0.0:20000".parse()?;
    let server = SensorServer::new(client).await?;
    let svc = InterceptedService {
        inner: crate::theaterops::sensors::sensor_server::SensorServer::new(server),
    };

    tonic::transport::Server::builder()
        .add_service(svc)
        .serve(addr)
        .await?;
    Ok(())
}
*/
