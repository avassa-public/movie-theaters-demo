use warp::Filter;

struct State {
    registry: prometheus::Registry,
    message_cntr: prometheus::IntCounter,
    avassa_client: avassa_client_utilities::Client,
    process_collector: prometheus::process_collector::ProcessCollector,
}

impl State {
    fn new(avassa_client: avassa_client_utilities::Client) -> anyhow::Result<Self> {
        let registry = prometheus::Registry::new();
        let message_cntr = prometheus::IntCounter::new("messages", "Number of messages received")?;
        registry.register(Box::new(message_cntr.clone()))?;
        Ok(Self {
            registry,
            avassa_client,
            message_cntr,
            process_collector: prometheus::process_collector::ProcessCollector::for_self(),
        })
    }
}

#[derive(Debug)]
struct PrometheusError(String);

impl warp::reject::Reject for PrometheusError {}

async fn get_metrics(state: std::sync::Arc<State>) -> std::result::Result<String, warp::Rejection> {
    use prometheus::{core::Collector, Encoder};

    let te = prometheus::TextEncoder::new();
    let metrics = state.registry.gather();
    let mut buffer = Vec::new();
    if let Err(e) = te.encode(&metrics, &mut buffer) {
        return Err(warp::reject::custom(PrometheusError(format!("{:?}", e))));
    }
    let metrics = state.process_collector.collect();
    if let Err(e) = te.encode(&metrics, &mut buffer) {
        return Err(warp::reject::custom(PrometheusError(format!("{:?}", e))));
    }

    Ok(String::from_utf8_lossy(&buffer).to_string())
}

async fn dc_volga_listener(state: std::sync::Arc<State>, dc_name: String) {
    let mut metrics = std::collections::HashMap::new();
    loop {
        let consumer = state
            .avassa_client
            .volga_open_nat_consumer(
                "sensor-agg",
                super::SENSOR_TOPIC,
                &dc_name,
                avassa_client_utilities::volga::ConsumerOptions {
                    // position: avassa_client::volga::ConsumerPosition::Beginning,
                    position: avassa_client_utilities::volga::ConsumerPosition::Unread,
                    ..Default::default()
                },
            )
            .await;
        log::info!("Successfully opened consumer to {}", dc_name);
        if let Ok(mut consumer) = consumer {
            loop {
                let msg = consumer.consume().await;

                state.message_cntr.inc();

                let (_, msg) = if msg.is_err() {
                    break;
                } else {
                    msg.unwrap()
                };
                let sensor = serde_json::from_slice::<crate::sensor_common::SensorValue>(&msg);
                log::info!("Received message: {:#?}", &sensor);
                if let Ok(sensor) = sensor {
                    let key = (sensor.sensor_name.clone(), sensor.datacenter.clone());
                    let metric = metrics.entry(key).or_insert_with(|| {
                        let opts = prometheus::Opts::new(&sensor.sensor_name, &sensor.sensor_name)
                            .const_labels(
                                [("dc".to_string(), sensor.datacenter.clone())]
                                    .iter()
                                    .cloned()
                                    .collect(),
                            ); //.namespace(dc);
                        let gauge = prometheus::Gauge::with_opts(opts).unwrap();

                        let _ = state.registry.register(Box::new(gauge.clone()));
                        gauge
                    });
                    metric.set(sensor.value as f64);
                    //
                }
            }
        }

        tokio::time::sleep(std::time::Duration::from_secs(10)).await;
    }
}

pub(crate) async fn run(client: avassa_client_utilities::Client) -> anyhow::Result<()> {
    log::info!("starting {}", module_path!());
    let this_site = avassa_client_utilities::state::site_name(&client).await?;
    let dcs = avassa_client_utilities::state::assigned_sites(&client)
        .await?
        .into_iter()
        .filter(|site| &site.name != &this_site)
        .map(|site| site.name)
        .collect::<Vec<String>>();

    log::info!("Will check dcs: {:?}", &dcs);

    let state = std::sync::Arc::new(State::new(client)?);

    for dc in &dcs {
        let state = state.clone();
        let udc_name = dc.to_string();
        tokio::spawn(async move { dc_volga_listener(state, udc_name).await });
    }

    // Create Warp server
    let metrics = warp::path("metrics")
        .and(warp::get())
        .map(move || state.clone())
        .and_then(get_metrics);

    let addr: std::net::SocketAddr = "0.0.0.0:9000".parse()?;

    warp::serve(metrics).run(addr).await;
    Ok(())
}
