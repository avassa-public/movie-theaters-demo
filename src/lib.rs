mod theaterops;

pub mod sensor_common {
    use serde::{Deserialize, Serialize};

    #[derive(Debug, Deserialize, Serialize)]
    pub struct SensorValue {
        pub datetime: String,
        pub site: String,
        pub sensor_name: String,
        pub value: f64,
    }
}

pub const POPCORN_TOPIC: &str = "popcorns-popped";

pub mod otel {
    use anyhow::Context;
    //use opentelemetry::trace::TraceContextExt;
    use tracing_opentelemetry::OpenTelemetrySpanExt;
    use tracing_subscriber::prelude::*;

    pub fn init_tracing(service_name: String) -> anyhow::Result<()> {
        opentelemetry::global::set_text_map_propagator(
            opentelemetry_sdk::propagation::TraceContextPropagator::new(),
        );

        opentelemetry::global::set_error_handler(|error| {
            tracing::error!(error = format!("{error:#}"), "otel error")
        })
        .context("set error handler")?;
        tracing_subscriber::registry()
            .with(tracing_subscriber::EnvFilter::from_default_env())
            .with(
                tracing_subscriber::fmt::layer()
                    .json()
                    .with_span_list(false),
            )
            .with(otlp_layer(service_name)?)
            .try_init()
            .context("initialize tracing subscriber")
    }

    fn otlp_layer<S>(service_name: String) -> anyhow::Result<impl tracing_subscriber::Layer<S>>
    where
        S: tracing::Subscriber + for<'span> tracing_subscriber::registry::LookupSpan<'span>,
    {
        use opentelemetry::trace::TracerProvider;

        let exporter = opentelemetry_otlp::new_exporter().tonic();

        let trace_config = opentelemetry_sdk::trace::Config::default().with_resource(
            opentelemetry_sdk::Resource::new(vec![opentelemetry::KeyValue::new(
                "service.name",
                service_name,
            )]),
        );

        let tracer = opentelemetry_otlp::new_pipeline()
            .tracing()
            .with_exporter(exporter)
            .with_trace_config(trace_config)
            .install_batch(opentelemetry_sdk::runtime::Tokio)
            .context("install tracer")?
            .tracer("movie-theater");

        let layer = tracing_opentelemetry::layer().with_tracer(tracer);
        Ok(layer)
    }
    pub fn axum_make_span<B>(request: &axum::http::Request<B>) -> tracing::Span {
        let headers = request.headers();
        tracing::info_span!(
            "incoming request",
            ?headers,
            trace_id = tracing::field::Empty
        )
    }

    pub fn axum_accept_trace<B>(
        request: opentelemetry_http::Request<B>,
    ) -> opentelemetry_http::Request<B> {
        // Current context, if no or invalid data is received.
        let parent_context = opentelemetry::global::get_text_map_propagator(|propagator| {
            propagator.extract(&opentelemetry_http::HeaderExtractor(request.headers()))
        });
        tracing::Span::current().set_parent(parent_context);

        request
    }

    /// Recorcd the OTel trace ID of the given request as "trace_id" field in the current span.
    pub fn axum_record_trace_id<B>(
        request: opentelemetry_http::Request<B>,
    ) -> opentelemetry_http::Request<B> {
        use opentelemetry::trace::TraceContextExt;

        let span = tracing::Span::current();

        let trace_id = span.context().span().span_context().trace_id();
        span.record("trace_id", trace_id.to_string());

        request
    }
}

pub mod axum {
    pub async fn shutdown_signal() {
        let mut term = tokio::signal::unix::signal(tokio::signal::unix::SignalKind::terminate())
            .expect("install SIGTERM handler");
        let ctrl_c = tokio::signal::ctrl_c();
        tokio::select! {
            _ = term.recv() => {},
            _ = ctrl_c => {},
        };
    }

    pub struct AppError(anyhow::Error);

    impl axum::response::IntoResponse for AppError {
        fn into_response(self) -> axum::response::Response {
            (
                axum::http::StatusCode::INTERNAL_SERVER_ERROR,
                format!("Error: {}", self.0),
            )
                .into_response()
        }
    }

    impl<E> From<E> for AppError
    where
        E: Into<anyhow::Error>,
    {
        fn from(value: E) -> Self {
            Self(value.into())
        }
    }
}

#[derive(serde::Deserialize, Debug)]
pub struct ShowMovieRequest {
    pub movie_name: String,
}

#[derive(serde::Deserialize, serde::Serialize, Debug)]
pub struct ShowMovieResponse {
    pub movie_name: String,
    pub timestamp: u64,
    pub start: bool,
}
