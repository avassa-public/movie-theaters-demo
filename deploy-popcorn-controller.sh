#!/bin/bash

cred_dir=${1:-$HOME/.supctl-topdc}
tenant="popcorn-systems"

sites="stockholm-sture stockholm-sergel helsingborg-roda-kvarn gothenburg-bergakungen"
for s in $sites; do
  echo "name: $s" | supctl -d $cred_dir create tenants $tenant assigned-sites
done

for s in stockholm-sture stockholm-sergel; do
  supctl -d $cred_dir update config system sites "$s" <<EOF
labels:
  city: stockholm
EOF
done

cred_dir=${2:-$HOME/.supctl-popcorn-systems}

supctl -d $cred_dir create registry remote <<EOF
name: default
address: registry.gitlab.com
EOF

# supctl -v -d $cred_dir create strongbox identity approle <<EOF
# name: kettle-popper-manager
# weak-secret-id: true
# EOF

supctl -d $cred_dir create applications < popcorn-controller.app.yml
supctl -d $cred_dir create application-deployments < popcorn-controller.dep.yml
