variable "CI_REGISTRY_IMAGE" {
    default = "test"
}
variable "CI_COMMIT_REF_SLUG" {
    default = "test"
}

target "build" {
    cache-from = ["type=registry,ref=${CI_REGISTRY_IMAGE}:${CI_COMMIT_REF_SLUG}-cache"]
    cache-to = ["type=registry,ref=${CI_REGISTRY_IMAGE}:${CI_COMMIT_REF_SLUG}-cache"]
    target = "build"
}

target "app" {
    platforms = ["linux/amd64", "linux/arm64"]
    target = "app"
}

target "final" {
    platforms = ["linux/amd64", "linux/arm64"]
    name = "${app}-${replace(version, ".", "-")}"
    dockerfile = "Dockerfile"
    contexts = {
        build = "target:build",
        app = "target:app"
    }
    matrix = {
        app = [
            "curtain-controller",
            "visitor-counter",
            "digital-assets-manager",
            "kettle-popper-manager",
            "projector-operations"
        ]
        version = [
            "v1.0",
            "v2.0"
        ]
    }
    tags = [
        "${CI_REGISTRY_IMAGE}/${app}:${version}",
        "avassasystems/${app}:${version}"
    ]
    args = {
        APP = "${app}"
        VERSION = "${version}"
    }
    target = "final"
    output = ["type=registry"]
}

target "tensorflow" {
    platforms = ["linux/amd64", "linux/arm64"]
    name = "${app}-${replace(version, ".", "-")}"
    dockerfile = "Dockerfile-ml"
    matrix = {
        app = [
            "visitor-detection"
        ]
        version = [
            "v1.0",
            "v2.0"
        ]
    }
    tags = [
        "${CI_REGISTRY_IMAGE}/${app}:${version}",
        "avassasystems/${app}:${version}"
    ]
    args = {
        VERSION = "${version}"
    }
    output = ["type=registry"]
}
