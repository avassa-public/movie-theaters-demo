# Use the popcorn controller to test observability

## App spec
See [popcorn-controller.app.yml](popcorn-controller.app.yml).

## Ingress IP address
To get the ingress IP address on a certain site:
```shell
supctl -j show --site sthlm applications popcorn-controller service-instances | jq -r '.[0].ingress.ips[0]'
```

Can be used in a shell script, e.g.:
```shell
INGRESS=$(supctl -j show --site sthlm applications popcorn-controller service-instances | jq -r '.[0].ingress.ips[0]')

curl http://$INGRESS:10000/ready
```

## Readiness
### Get
```
curl -v http://$INGRESS:10000/ready
```
Returns 200 if ready, 204 if inactive, or 503 if not ready

### Post
```
curl -H "content-type: application/json" http://$INGRESS:10000/ready -d '{"ready": "ready"}'
curl -H "content-type: application/json" http://$INGRESS:10000/ready -d '{"ready": "inactive"}'
curl -H "content-type: application/json" http://$INGRESS:10000/ready -d '{"ready": "fail"}'
```

## Liveness
### Get
```
curl -v http://$INGRESS:10000/live
```
Returns 200 if ready, or 503 if not live

### Post
```
curl -H "content-type: application/json" http://$INGRESS:10000/live -d '{"live": false}'
```

## Graceful shutdown
Will shutdown the container with exit code 0, post anything here.
```
curl http://$INGRESS:10000/shutdown -d ''
```

## Crash
Will shutdown the container with exit code 123, post anything here.
```
curl http://$INGRESS:10000/crash -d ''
```

## Memory Consumption
Ask container to allocate memory
```
curl -H "content-type: application/json" http://$INGRESS:10000/memory -d '{"mb": 200}'
```

To release memory, set `mb: 0`.
