#!/bin/bash

cred_dir=${1:-$HOME/.supctl-topdc}

tenant="theater-operation"

sites="stockholm-sture stockholm-sergel helsingborg-roda-kvarn gothenburg-bergakungen"
 for s in $sites; do
    echo "name: $s" | supctl -d "$cred_dir" create tenants $tenant assigned-sites
 done

cred_dir=${2:-$HOME/.supctl-theater-operation}

supctl -d "$cred_dir" create strongbox vaults <<EOF
name: operations
distribute:
  to: all
EOF

supctl -d "$cred_dir" create strongbox vaults operations secrets <<EOF
name: credentials
allow-image-access:
  - "*"
data:
  username: test
  password: secret
EOF

supctl -d "$cred_dir" create strongbox authentication approles <<EOF
name: projector-operations
weak-secret-id: true
EOF

supctl -d "$cred_dir" replace policy policies digital-assets-manager <<EOF
name: digital-assets-manager
rest-api:
  rules:
    - path: /v1/*/system/cluster/**
      operations:
        read: allow
    - path: /v1/state/strongbox/vaults/operations/**
      operations:
        read: allow
    - path: /v1/state/system/sites
      operations:
        read: allow
    - path: /v1/state/system/sites/**
      operations:
        read: allow
EOF

supctl -d "$cred_dir" delete strongbox authentication approles digital-assets-manager > /dev/null 2>&1
supctl -d "$cred_dir" create strongbox authentication approles <<EOF
name: digital-assets-manager
weak-secret-id: true
token-policies:
  - digital-assets-manager
EOF

supctl -d "$cred_dir" create strongbox authentication approles <<EOF
name: curtain-controller
weak-secret-id: true
EOF

supctl -d "$cred_dir" create applications < theater-room-manager-v1.0.app.yml
supctl -d "$cred_dir" create application-deployments < theater-room-manager.dep.yml
